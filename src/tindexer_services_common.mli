(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Alpha_environment
open Alpha_context

val derive_key :
  ([ `GET ],
   unit,
   unit * Bip32_ed25519.Human_readable.node list,
   unit,
   unit, Ed25519.Public_key_hash.t) RPC_service.service

val history :
  ([ `GET ],
   unit,
   unit,
   Contract.contract list,
   unit,
   Chain_db.tx_full list list) RPC_service.service

val contracts :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Contract.t list list) RPC_service.service

val contract_info :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.Contract_table.t option list) RPC_service.service

val missing_blocks :
  ([ `GET ],
   unit,
   unit,
   unit,
   unit,
   int32 list) RPC_service.service

val stake :
  ([ `GET ],
   unit,
   (unit * public_key_hash) * Contract.t,
   unit,
   unit,
   Tindexer_stake.fulldiff list) RPC_service.service
