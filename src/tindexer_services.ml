(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let forge_transfer =
  RPC_service.post_service
    ~description: "Forge a transfer operation"
    ~query: RPC_query.empty
    ~input: Tindexer_transfer.(manager_operations_encoding destination_encoding)
    ~output: Tindexer_transfer.forged_mgr_op_encoding
    RPC_path.(root / "forge_transfer")

let originate_account =
  RPC_service.post_service
    ~description:"Originate an account"
    ~query: RPC_query.empty
    ~input: Tindexer_transfer.(manager_operations_encoding origination_encoding)
    ~output: Tindexer_transfer.forged_mgr_op_encoding
    RPC_path.(root / "originate_account")

let change_delegate =
  RPC_service.post_service
    ~description:"Change or remove the delegate of an account"
    ~query: RPC_query.empty
    ~input: (Tindexer_transfer.manager_operations_encoding
               Signature.Public_key_hash.encoding)
    ~output: Tindexer_transfer.forged_mgr_op_encoding
    RPC_path.(root / "change_delegate")
