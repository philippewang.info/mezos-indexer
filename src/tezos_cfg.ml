(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let src = Logs.Src.create "tindexer.tezos_cfg"

let host_port_of_url url =
  let tls = match Uri.scheme url with
    | None -> false
    | Some "https" -> true
    | Some _ -> false in
  match Uri.host url, Uri.port url with
  | Some host, Some port -> host, port, tls
  | _ -> invalid_arg "host_port_of_url"

let base_dir = Filename.concat (Sys.getenv "HOME") ".tezos-client"
let cfg_file base_dir = Filename.concat base_dir "config"

let mk_rpc_cfg tezos_client_dir url =
  let cfg = cfg_file tezos_client_dir in
  Client_config.read_config_file cfg >>= fun c ->
  match c, Option.map ~f:host_port_of_url url with
  | Error e, None ->
    Logs.err ~src begin fun m ->
      m "Error opening %s: %a" cfg pp_print_error e
    end ;
    invalid_arg "read_cfg_file: no file nor cmdline argument provided"
  | Error _, Some (host, port, tls) ->
    return ({ RPC_client.default_config with host ; port ; tls },
            None)
  | Ok _, Some (host, port, tls) ->
    return ({ RPC_client.default_config with host ; port ; tls },
            None)
  | Ok { node_addr; node_port; tls; confirmations }, None ->
    return ({ RPC_client.default_config with host = node_addr ;
                                             port = node_port ; tls },
            confirmations)

let tindexer_full
    ?(block=`Head 0)
    ?confirmations
    ?password_filename
    ?(base_dir=base_dir)
    ?(rpc_config=RPC_client.default_config)
    () : #Client_context.full = object
  inherit Client_context_unix.unix_full
      ~chain:`Main ~base_dir ~block ~confirmations ~password_filename ~rpc_config
  method answer :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.app ~src (fun m -> m "%s" msg) ;
        Lwt.return_unit)
  method error :
    type a b. (a, b) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.err (fun m -> m "%s" msg) ;
        Lwt.fail_with msg)
  method log : (* never used in practice *)
    type a. string -> (a, unit) Client_context.lwt_format -> a =
    fun _output -> Format.kasprintf (fun msg ->
        Logs.info (fun m -> m "%s" msg) ;
        Lwt.return_unit)
  method message :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.app ~src (fun m -> m "%s" msg) ;
        Lwt.return_unit)
  method warning :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.warn (fun m -> m "%s" msg) ;
        Lwt.return_unit)
end
