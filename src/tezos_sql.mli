(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Caqti_type

(** Helpers *)

val connect : Uri.t -> (module Caqti_lwt.CONNECTION) Lwt.t

val to_caqti_error : ('a, error list) result -> ('a, string) result

val with_transaction :
  (module Caqti_lwt.CONNECTION) ->
  ('a, unit, [< `Zero ]) Caqti_request.t -> 'a list -> unit Lwt.t

(** Shell encoders *)

val z : Z.t t
val json : Data_encoding.json t
val oph : Operation_hash.t t
val pkh : Signature.Public_key_hash.t t
val pk : Signature.Public_key.t t

val chain_id : Chain_id.t t
val blk_hash : Block_hash.t t
val context_hash : Context_hash.t t
val operation_list_hash : Operation_list_hash.t t
val operation_list_list_hash : Operation_list_list_hash.t t
val time : Time.t t
val shell_header : Block_header.shell_header t

(** Proto encoders *)

open Alpha_context
open Caqti_type

val k : Contract.t t
val kh : Contract_hash.t t
val tez : Tez.t t
val lazy_expr : Script.lazy_expr t
val script : Script.t t
val balance : Delegate.balance t
val balance_update : Delegate.balance_update t
val cycle : Cycle.t t
val voting_period : Voting_period.t t
val voting_period_kind : Voting_period.kind t

type delegate_info = {
  cycle: Cycle.t ;
  level: int32 ;
  pkh: public_key_hash ;
  info: Delegate_services.info ;
}

val delegate_info : delegate_info t
