(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Alpha_context

val default_fee_parameter : Injection.fee_parameter
(** [default_fee_parameter] is suitable to send a transaction minimal
    as currently accepted by most nodes. If unsure, do not use and set
    your own fees. *)

type destination = {
  destination : Contract.t ;
  amount : Tez.t ;
  parameters : Script.lazy_expr option ;
}

val create_destination :
  ?parameters:Script.lazy_expr ->
  destination:Contract.t -> amount:Tez.t -> unit -> destination

val pp_destination : Format.formatter -> destination -> unit
val destination_encoding : destination Data_encoding.t

type 'a manager_operations = {
  src : Contract.t ;
  src_pk : Signature.public_key ;
  ops : 'a list ;
}

val pp_manager_operations :
  (Format.formatter -> 'a -> unit) ->
  Format.formatter -> 'a manager_operations -> unit

val create_transfer :
  src:Contract.t ->
  src_pk:Signature.public_key ->
  dsts:destination list ->
  destination manager_operations

val manager_operations_encoding :
  'a Data_encoding.t ->
  'a manager_operations Data_encoding.t

type forged_mgr_op = {
  payload : MBytes.t ;
  fees : Tez.t ;
  gas : Z.t ;
}

val forged_mgr_op_encoding : forged_mgr_op Data_encoding.t

val forge_transfer :
  ?fee_parameter:Injection.fee_parameter ->
  ?pk:public_key ->
  #full ->
  Contract.t ->
  destination list ->
  forged_mgr_op tzresult Lwt.t
(** [forge_transfer ?fee_parameter ?pk cctxt k dsts] returns a tuple
    where the first element is the serialized data structure for a
    simple transfer from [k] to [dsts] using [pk] as [k]'s public key,
    and the second element is the computed fees that most node will
    require to process the transfer. *)

type origination = {
  manager: Signature.Public_key_hash.t ;
  delegate: Signature.Public_key_hash.t option ;
  script: Script.t option ;
  credit: Tez.t ;
  delegatable: bool ;
}

val origination_encoding : origination Data_encoding.t

val forge_origination :
  ?fee_parameter:Injection.fee_parameter ->
  #full ->
  Contract.t ->
  public_key ->
  origination list ->
  forged_mgr_op tzresult Lwt.t
(** [forge_origination ?fee_parameter cctxt k pk origs] returns a
    serialized data structure for a simple origination of [origs]
    from [k] using [pk] as [k]'s public key. *)

val forge_delegation :
  ?fee_parameter:Injection.fee_parameter ->
  #full ->
  Contract.t ->
  public_key ->
  public_key_hash option ->
  forged_mgr_op tzresult Lwt.t
(** [forge_delegation ?fee_parameter cctxt k pk delegate] returns a
    serialized data structure for a delegation to [delegate]
    from [k] using [pk] as [k]'s public key. *)
