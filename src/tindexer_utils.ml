(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module IntMap = Map.Make(struct
    type t = int
      let compare = Pervasives.compare end)

module FloatMap = Map.Make(struct
    type t = float
    let compare = Pervasives.compare end)

module Cache = struct
  type 'a t = {
    max_size : int ;
    mutable cache : 'a FloatMap.t ;
    mutable idx : int ;
  }

  let create ?(max_size=10) () =
    { max_size ;
      cache = FloatMap.empty ;
      idx = 0 }

  let find_opt { cache ; _ } k =
    let cur_ts = Unix.gettimeofday () in
    let v = ref None in
    try
      FloatMap.fold begin fun ts (cur_k, history) () ->
        if cur_k = k && cur_ts -. ts < 600. then begin
          v := Some history ;
          raise Exit
        end
      end cache () ;
      None
    with Exit -> !v

  let add t k history =
    if t.idx < t.max_size then begin
      t.cache <- FloatMap.add (Unix.gettimeofday ()) (k, history) t.cache ;
      t.idx <- t.idx + 1
    end
    else begin
      let new_cache =
        FloatMap.remove
          (fst (FloatMap.min_binding t.cache)) t.cache in
      t.cache <-
        FloatMap.add (Unix.gettimeofday ()) (k, history) new_cache ;
      t.idx <- t.idx + 1
    end
end
