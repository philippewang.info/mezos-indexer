(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let setup_log level logs =
  Logs.set_level level ;
  Logs.set_reporter (Logs_lwt_ovh.udp_reporter ?logs ())

open Cmdliner
open Rresult

module Convs = struct
  let uri =
    let parser v = R.ok (Uri.of_string v) in
    Arg.conv ~docv:"URL" (parser, Uri.pp_hum)

  let bip32_node =
    let bip32_node_of_string v =
      match Int32.of_string v with
      | exception _ -> begin
          match Int32.of_string String.(sub v 0 (length v - 1)) with
          | exception _ -> Error "Invalid BIP32 node"
          | v -> Ok (Int32.logor 0x8000_0000l v)
        end
      | v -> Ok v in
    let pp_bip32_node ppf node =
      if node < 0l then
        Format.fprintf ppf "%ld'" Int32.(add min_int node)
      else
        Format.fprintf ppf "%ld" node in
    let parser v =
      R.reword_error (fun s -> `Msg s) (bip32_node_of_string v) in
    let printer = pp_bip32_node in
    Arg.conv ~docv:"BIP32 NODE" (parser, printer)

  let bip32_path =
    let open Bip32_ed25519.Human_readable in
    let parser v =
      match path_of_string v with
      | None -> Error (`Msg "Invalid BIP32 path")
      | Some path -> Ok path in
    Arg.conv ~docv:"BIP32 PATH" (parser, pp_path)
end

module Terms = struct
  let ovh_log =
    let doc = "URL and token for OVH Logs service." in
    Arg.(value
         & opt (some (t2 Convs.uri string)) None
         & info ["ovh-logs"] ~doc ~docv: "URL,TOKEN")

  let setup_log =
    Term.(const setup_log $ Logs_cli.level () $ ovh_log)

  let tezos_client_dir =
    Arg.(value
         & opt string (Filename.concat (Sys.getenv "HOME") ".tezos-client")
         & info ["tezos-client-dir"]
           ~doc:"Where Tezos client config resides"
           ~docv:"PATH")

  let datadir =
    Arg.(value
         & opt string (Filename.concat (Sys.getenv "HOME") ".tindexer")
         & info ["d"; "datadir"]
           ~doc:"Where Tindexer data will be stored"
           ~docv:"PATH")

  let force ~doc =
    Arg.(value & flag & info ["f"; "force"] ~doc)

  let host ?(argnames=["h"; "host"]) ~default ~doc () =
    Arg.(value & opt string default & info argnames ~doc ~docv:"HOST")

  let port ?(argnames=["p"; "port"]) ~default ~doc () =
    Arg.(value & opt int default & info argnames ~doc ~docv:"PORT")

  let cfg_file =
    let base_dir = Filename.concat (Sys.getenv "HOME") ".tezos-client" in
    let default = Filename.concat base_dir "config" in
    let doc = "`tezos-client` config file" in
    Arg.(value & opt string default & info ["c"; "cfg"] ~doc ~docv:"FILENAME")

  let uri ~default ~doc ~args =
    Arg.(value & opt Convs.uri default & info args ~doc ~docv: "URL")

  let uri_option ?default ~doc ~args () =
    Arg.(value & opt (some Convs.uri) default & info args ~doc ~docv: "URL")

  let db ~default =
    uri ~default ~doc:"Database to connect to" ~args:["db"]

  let tls =
    Arg.(value & flag &
         info ["s"; "tls"] ~doc:"use TLS to connect to Tezos node")

  let alias_pos position =
    let doc = "Wallet name to use." in
    Arg.(required & pos position (some string) None & info [] ~docv:"WALLET" ~doc)

  let alias_opt =
    let doc = "Wallet name to use." in
    Arg.(value & opt (some string) None & info ["a"; "alias"] ~docv:"WALLET" ~doc)
end
